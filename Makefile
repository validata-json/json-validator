.PHONY: help test build build-api build-cli generate-api-boilerplate generate-examples

help:
	@ grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

test: ## Run all tests of the module and its submodules
	@ go test ./...

build: build-api build-cli ## Builds all binaries (API, cli)

build-api: ## Builds the binary that serves the API
	@ go build  -o validation-api ./cmd/validation

build-cli: ## Builds the validation command line client
	@ goreleaser release --snapshot --skip-publish --rm-dist

generate-api-boilerplate: ## Use after any change to design/design.go to update generated files accordingly. Also updates documentation static site
	@ goa gen json-validator/design
	@ cp ./gen/http/openapi3.yaml ./docs/openapi3.yaml

generate-examples: ## Updates examples in validator/error_formatting_test.go
	@ go generate ./...

