/!\ Dépôt archivé, supplanté par [celui-ci](https://git.opendatafrance.net/validata/validata-json/).

# Validation de données JSON avec des rapports de validation en français

Ce dépôt implémente deux outils pour la validation de données JSON en 
français :

* Une API
* Un outil en ligne de commandes

Les deux outils permettent la validation d'un fichier au format JSON via un 
schéma au format [JSON schema](https://json-schema.org/). La valeur de retour 
est (l'un ou l'autre au choix pour l'outil en ligne de commande) :

* le tableau de toutes les erreurs de validation et leurs détails (format 
  JSON);
* un rapport d'erreur qui décrit les erreurs de validation en regroupant les 
  erreurs identiques (format plain text).

## Types de sortie
### Rapport d'erreur

Le rappor d'erreur va tenter de regrouper les erreurs identiques en regroupant 
les erreurs du même types, survenues sur des contextes qui ne différent que 
par une position dans un tableau (un array json).

Un seul paragraphe consolidé sera consacré à ces erreurs regroupées, qui se 
présentera sous la forme textuelle suivante (numérotation ajoutée pour 
l'exemple) :

```
1. Erreur survenue au niveau de l'attribut "trafic_vit"
2. Position                : (racine).features.1.properties.trafic_vit
3. Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) nombre entier est attendu(e).
4. Valeur de l'objet       : null
5. Code erreur             : invalid_type
6. Erreur similaire sur cet attribut constatée à 3 autre(s) position(s)
7.  (racine).features.4.properties.trafic_vit
8.  (racine).features.13.properties.trafic_vit
9.  (racine).features.20.properties.trafic_vit
```

Ce bloc présente successivement :
- le nom de l'attribut et sa position dans l'objet (l1-2)
- une description de l'erreur et un code standard associé (l3 et l5)
- la valeur de l'objet qui a occasionné l'erreur (l4, tronqué si la 
  représentation textuelle de l'objet est longue avec la mention "...OBJET 
  TRONQUÉ")
- La mention (le cas échéant) d'erreurs similaires (même type, position qui 
  varie uniquement par un index différent dans un array) (l6)
- Les positions des autres erreurs, uniquement si le nombre total d'erreurs 
  regroupées est inférieur à 5. (l7-9)
  
### Données d'erreur de validation brutes

Les données brutes de validation renvoyées est un objet json avec les 
propriétés suivantes :
  * `field`: nom du champ qui a déclenché l'erreur
  * `type`: type d'erreur de validation
  * `context`: chemin du champ qui a déclenché l'erreur
  * `description`: description textuelle de l'erreur
  * `value`: la valeur du champ qui a déclenché l'erreur
  * `details`: tableau de détails concernant l'erreur. Dépend du type 
    d'erreur.
  
## Fonctionnement 

### API

Pour lancer l'API en local, lancer l'exécutable `validation-api`.
La documentation de l'API est disponible 
[ici](http://json.validator.validata.fr/apidocs/). L'api est déployée sur 
[json.validator.validata.fr/](http://json.validator.validata.fr).

Exemple de requête :
```
http://json.validator.validata.fr/validate?data=https%3A%2F%2Fwww.data.gouv.fr%2Ffr%2Fdatasets%2Fr%2F9ca17d67-3ba3-410b-9e32-6ac7948c3e06&schema=https%3A%2F%2Fschema.data.gouv.fr%2Fschemas%2Fetalab%2Fschema-amenagements-cyclables%2F0.3.3%2Fschema_amenagements_cyclables.json&raw=false
```

### Outil en ligne de commande

L'outil en ligne de commande [peut être téléchargé ici](https://git.opendatafrance.net/outillages/json-validator/-/jobs/artifacts/main/browse?job=release) 
pour Linux, MacOS et Windows. 

Il renvoie par défaut le rapport d'erreur à l'usage d'un humain,
mais peut également retourner du json pour consommation par une autre 
application avec le drapeau `--raw`.

L'aide et les options peuvent être affichées en ajoutant le drapeau `--help`.

Exemple d'usage, après avoir téléchargé le fichier correspondant à votre 
architecture :

```
 # Après téléchargement du client en ligne de commande "validation-cli" dans le répertoire courant
 chmod +x validation-cli
 # Téléchargement de données d'aménagements cyclables
 wget https://www.data.gouv.fr/fr/datasets/r/54f9ee93-76e4-4e02-afbf-7dd0c954355a -O amenagementcyclable_eurometropolemetz.geojson
 # Validation. Le schéma comme les données peuvent être locaux ou distants
 ./validation-cli -data amenagementcyclable_eurometropolemetz.geojson -schema https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json
```

## Détails d'implémentation

L'API a été implémentée avec [Goa](https://goa.design/), dans lequel l'API est 
définie via un [design](./design/design.go), qui permet ensuite de générer une 
grande partie du code (répertoire [gen](./gen/)).

L'outil en ligne de commande est défini dans le répertoire 
[./cmd/validation-cli-standalone/](./cmd/validation-cli-standalone).

Le module [validator](./validator/) définit le code métier de validation de 
données json et de rapport d'erreur en français, à la fois utilisé par l'API 
et par l'outil en ligne de commande.

Le répertoire [./docs](docs) est issu de 
[swagger-ui](https://swagger.io/tools/swagger-ui/).
