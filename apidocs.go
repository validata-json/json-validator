package validationapi

import (
	apidocs "json-validator/gen/apidocs"
	"log"
)

// apidocs service example implementation.
// The example methods log the requests and return zero values.
type apidocssrvc struct {
	logger *log.Logger
}

// NewApidocs returns the apidocs service implementation.
func NewApidocs(logger *log.Logger) apidocs.Service {
	return &apidocssrvc{logger}
}
