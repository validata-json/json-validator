package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"json-validator/utils"
	"json-validator/validator"
	"os"
	"strings"

	"github.com/pkg/errors"
)

func main() {
	var (
		schemaF = flag.String("schema", "", "Schema file `path` (local or url)")
		dataF   = flag.String("data", "", "Data file `path` (local or url)")
		rawF    = flag.Bool("raw", false, "If set, returns json validation errors rather than human readable report.")
	)
	flag.Usage = usage
	flag.Parse()

	schemaURI := *schemaF
	dataURI := *dataF

	// var err error = nil
	// if err != nil {
	// 	if err == flag.ErrHelp {
	// 		os.Exit(0)
	// 	}
	// 	fmt.Fprintln(os.Stderr, err.Error())
	// }
	if *schemaF == "" || *dataF == "" {
		fmt.Fprintln(os.Stderr, "run '"+os.Args[0]+" --help' for detailed usage.")
		os.Exit(1)
	}

	schemaContent, schemaErr := utils.FetchContent(schemaURI)
	exitIfErr(schemaErr, "An error occured while trying to open schema file")

	dataContent, dataErr := utils.FetchContent(dataURI)
	exitIfErr(dataErr, "An error occured while trying to open data file")

	vErrs, err := validator.Validate(schemaContent, dataContent)
	exitIfErr(err, "An error occured during data validation")

	if *rawF {
		jsonBytes, err := json.Marshal(utils.CastValidationErrors(vErrs, true).ValidationErrors)
		exitIfErr(err, "")

		fmt.Println(string(jsonBytes))
	} else {
		fmt.Println(vErrs.GenerateReport())
	}
	os.Exit(0)
}

func usage() {
	fmt.Fprintf(os.Stderr, "%s is a command line client for the validation API.\n\n", os.Args[0])
	fmt.Fprintln(os.Stderr, "Usage:")
	flag.PrintDefaults()
	fmt.Fprintln(os.Stderr, "")
	fmt.Fprintf(os.Stderr, `Example:
%s
`, indent(usageExamples()))
}

func indent(s string) string {
	if s == "" {
		return ""
	}
	return "    " + strings.Replace(s, "\n", "\n    ", -1)
}

func usageExamples() string {
	return os.Args[0] +
		` \
	-schema https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json \
	-data https://www.data.gouv.fr/fr/datasets/r/b11dffe1-dcf9-438e-99bb-23cca0a6761a
	`
}

func exitIfErr(err error, msg string) {
	if err != nil {
		if msg == "" {
			fmt.Fprintln(os.Stderr, err.Error())
		} else {
			fmt.Fprintln(os.Stderr, errors.Wrap(err, msg))
		}
		os.Exit(1)
	}
}
