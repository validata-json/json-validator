package design

import . "goa.design/goa/v3/dsl"

var _ = API("validation", func() {
	Title("JSON Validation Service")
	Description("A service for validating JSON files with respect to a given JSON Schema.")

	Server("validation", func() {
		Description("Hosts the JSON validation service.")

		Services("validation")
		Services("apidocs")

		Host("public", func() {
			Description("Temporary host for testing.")
			URI("https://json.validator.validata.fr/")
		})
		Host("localhost", func() {
			Description("Development host.")
			URI("http://localhost:8000/")
		})
	})
})

var _ = Service("validation", func() {
	Description("The validation service validates JSON Files with respect to a given JSON Schema")

	Error("UnprocessableJsonSchema", func() {
		Description("UnprocessableJsonSchema is the error returned when the JSON Schema provided is valid json, but does not follow JSON Schema specifications.")
	})
	Error("UnsupportedMediaType", func() {
		Description("UnsupportedMediaType is the error returned when either files provided are not JSON.")
	})

	Method("validate-file", func() {
		Meta("swagger:summary", "Validate local data")
		Description("Validation of a local data file with a remote JSON schema")
		Payload(func() {
			Field(1, "data", String, "JSON file with data to validate",
				func() {
					Example("example data", `@local_data.json`)
				})
			Field(2, "schema", String, "JSON Schema file, MUST be in the format of an URL",
				func() {
					Example("amenagements cyclables", "https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json")
				})
			Field(3, "raw", Boolean, "Should the raw errors be returned in addition to the report. Beware, can result in large return value.",
				func() {
					Default(false)
					Example(false)
				})
			Required("data", "schema")
		})

		Result(ValidationResult)

		HTTP(func() {
			POST("/validate")
			MultipartRequest()

			Response(StatusOK, func() {
				ContentType("application/json")
			})

			Response("UnprocessableJsonSchema", StatusExpectationFailed)
			Response("UnsupportedMediaType", StatusUnsupportedMediaType)
		})
	})

	Method("validate-url", func() {
		Meta("swagger:summary", "Validate data with URL")
		Description("Validation of remote data with a remote JSON schema")
		Payload(func() {
			Field(1, "data", String, "JSON file with data to validate, MUST be in the format of an URL",
				func() {
					Example("https://www.data.gouv.fr/fr/datasets/r/9ca17d67-3ba3-410b-9e32-6ac7948c3e06")
				})
			Field(2, "schema", String, "JSON Schema file, MUST be in the format of an URL",
				func() {
					Example("https://schema.data.gouv.fr/schemas/etalab/schema-amenagements-cyclables/0.3.3/schema_amenagements_cyclables.json")
				})
			Field(3, "raw", Boolean, "Should the raw errors be returned in addition to the report. Beware, can result in large return value.",
				func() {
					Default(false)
					Example(false)
				})
			Required("data", "schema")
		})

		Result(ValidationResult)

		HTTP(func() {
			GET("/validate")
			Params(func() {
				Param("data")
				Param("schema")
				Param("raw")
			})

			Response(StatusOK, func() {
				ContentType("application/json")
			})

			Response("UnprocessableJsonSchema", StatusExpectationFailed)
			Response("UnsupportedMediaType", StatusUnsupportedMediaType)
		})
	})
})

var _ = Service("apidocs", func() {
	Meta("swagger:generate", "false")
	Description("The apidocs service serves the API swagger definition")
	HTTP(func() {
		Path("/apidocs")
	})
	Files("/{*path}", "./docs/", func() {
		Description("Serve documentation")
	})
})

var ValidationError = ResultType("ValidationError", func() {
	Description("All information to inspect a specific validation error.")
	TypeName("ValidationError")

	Attribute("field", String, "Name of field that triggered an error",
		func() { Example("\"bar\" field", "bar") })
	Attribute("type", String, "Type of error",
		func() { Example("example error type", "array_no_additional_items") })
	Attribute("context", String, "Tree like notation of the part that failed the validation",
		func() { Example("example context", "(racine).foo.0.bar") })
	Attribute("description", String, "Human-readable description of the error",
		func() {
			Example("example description", "Le tableau contient des éléments supplémentaires à ceux autorisés par le schéma.")
		})
	Attribute("value", Any, "Value of the field related to the error",
		func() { Example("example value", "[1,2,3,4]") })
	Attribute("details", MapOf(String, Any), "Additional details about the error")

	Required("field", "type", "context", "description", "value", "details")
})

var ValidationResult = ResultType("ValidationResult", func() {
	Description("Information if the JSON file is valid or not, the possible validation errors and a validation summary.")
	TypeName("ValidationResult")
	Attribute("isValid", Boolean, "Whether the input JSON is valid")
	Attribute("ValidationErrors", CollectionOf(ValidationError), "If any, and if raw errors have been asked for in query, information on validation errors.")
	Attribute("ValidationSummary", String, "Summary of validation errors.",
		func() {
			Example("example summary", `Erreur survenue au niveau de l'attribut "bar"
Position                : (racine).foo.0.bar
Description de l'erreur : Le tableau contient des éléments supplémentaires à ceux autorisés par le schéma.
Valeur de l'objet       : [1,2,3,4]
Code erreur             : array_no_additional_items
Erreur similaire sur cet attribut constatée à 1 autre(s) position(s)
	(racine).foo.3.bar
`)
		})
	Required("isValid", "ValidationErrors", "ValidationSummary")
})
