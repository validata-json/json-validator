// Code generated by goa v3.5.5, DO NOT EDIT.
//
// apidocs service
//
// Command:
// $ goa gen json-validator/design

package apidocs

// The apidocs service serves the API swagger definition
type Service interface {
}

// ServiceName is the name of the service as defined in the design. This is the
// same value that is set in the endpoint request contexts under the ServiceKey
// key.
const ServiceName = "apidocs"

// MethodNames lists the service method names as defined in the design. These
// are the same values that are set in the endpoint request contexts under the
// MethodKey key.
var MethodNames = [0]string{}
