package validationapi

import (
	"io"
	"io/ioutil"
	"json-validator/gen/validation"
	"mime/multipart"
)

// ValidationValidateFileDecoderFunc implements the multipart decoder for service
// "validation" endpoint "validate". The decoder must populate the argument p
// after encoding.
func ValidationValidateFileDecoderFunc(mr *multipart.Reader, p **validation.ValidateFilePayload) error {
	// Add multipart request decoder logic here
	r := validation.ValidateFilePayload{}
	for {
		part, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		switch part.FormName() {
		case "data":
			r.Data, err = readPartContent(part)
		case "schema":
			r.Schema, err = readPartContent(part)
		}
		if err != nil {
			return err
		}
	}
	*p = &r
	return nil
}

func readPartContent(part *multipart.Part) (string, error) {
	content, err := ioutil.ReadAll(part)
	if err != nil {
		return "", err
	}
	return string(content), nil
}

// ValidationValidateFileEncoderFunc implements the multipart encoder for service
// "validation" endpoint "validate".
func ValidationValidateFileEncoderFunc(mw *multipart.Writer, p *validation.ValidateFilePayload) error {

	defer mw.Close()
	err := mw.WriteField("schema", p.Schema)
	if err != nil {
		return err
	}
	err = writeFormFile(mw, "data", p.Data)
	if err != nil {
		return err
	}
	return nil
}

// writeFormFile writes a []byte object as a Form File with given
// `fieldname` to a multipart writer
func writeFormFile(mw *multipart.Writer, fieldname, file string) error {
	fw, err := mw.CreateFormFile(fieldname, "mockfilename.json")
	if err != nil {
		return err
	}
	_, err = fw.Write([]byte(file))
	if err != nil {
		return err
	}
	return nil
}
