package validationapi

import (
	"bytes"
	"fmt"
	"json-validator/gen/validation"
	"mime/multipart"
	"testing"

	"github.com/stretchr/testify/assert"
)

var defaultMultipartBoundary = "AaB03x"

// TestValidationValidateDecoderFunc tests if the multiform decoder works as
// expected
func TestValidationValidateDecoderFunc(t *testing.T) {
	mp := createTestMultipart("{test: 1}", "{test: 2}", defaultMultipartBoundary)
	payload, err := decodeToPayload(mp, defaultMultipartBoundary)

	assert.Equal(t, nil, err)
	testFileContent(t, "{test: 1}", payload.Schema)
	testFileContent(t, "{test: 2}", payload.Data)
}

func decodeToPayload(multipartBytes []byte, boundary string) (*validation.ValidateFilePayload, error) {
	mr := multipart.NewReader(bytes.NewReader(multipartBytes), defaultMultipartBoundary)
	payloadObject := &validation.ValidateFilePayload{}
	err := ValidationValidateFileDecoderFunc(mr, &payloadObject)
	return payloadObject, err
}

func TestValidationValidateEncoderFunc(t *testing.T) {

	payloadObject := &validation.ValidateFilePayload{
		Schema: "{test: 1}",
		Data:   "{test: 2}",
	}
	payloadBytes, err := encodeFromPayload(payloadObject, defaultMultipartBoundary)
	assert.Equal(t, nil, err)
	expectedMultipart := string(createTestMultipart("{test: 1}", "{test: 2}", defaultMultipartBoundary))
	assert.Equal(t, expectedMultipart, string(payloadBytes))
}

func encodeFromPayload(payloadObject *validation.ValidateFilePayload, boundary string) ([]byte, error) {
	payloadBytes := []byte{}
	payloadBuffer := bytes.NewBuffer(payloadBytes)
	mw := multipart.NewWriter(payloadBuffer)
	mw.SetBoundary(defaultMultipartBoundary)

	err := ValidationValidateFileEncoderFunc(mw, payloadObject)
	return payloadBuffer.Bytes(), err
}

// createTestMultipart Creates a hardcoded multipart (as []byte) with given content
// (associated to expected "schema" and "data" fields), and
// with given boundary.
func createTestMultipart(schemaContent, dataContent, boundary string) []byte {
	multipartString := fmt.Sprintf(
		"--%[1]s\r\n"+multipartFormFileHeader("schema", false)+"%[2]s\r\n--%[1]s\r\n"+multipartFormFileHeader("data", true)+"%[3]s\r\n--%[1]s--\r\n",
		// CRLF linebreak in spec : https://www.rfc-editor.org/rfc/rfc2616#section-2.2
		boundary,
		schemaContent,
		dataContent,
	)
	return []byte(multipartString)
}

func multipartFormFileHeader(field string, isFile bool) string {
	var fileSpecific string
	if isFile {
		fileSpecific = "; filename=\"mockfilename.json\"\r\nContent-Type: application/octet-stream"
	}
	return fmt.Sprintf("Content-Disposition: form-data; name=\"%s\""+fileSpecific+"\r\n\r\n", field)
}

func testFileContent(t *testing.T, expectedContent, file string) {
	t.Helper()
	assert.Equal(t, expectedContent, file)
}
