package utils

import (
	"json-validator/gen/validation"
	"json-validator/validator"
)

// castValidationErrors casts a collection of `ValidationError`s from the
// validator package into the `ValidationResult` type expected as the API return
// type.
func CastValidationErrors(vErrs validator.ValidationErrors, keepRaw bool) *validation.ValidationResult {
	cast := &validation.ValidationResult{}
	cast.IsValid = !vErrs.HasAnyError()
	cast.ValidationSummary = vErrs.GenerateReport()

	if keepRaw {
		cast.ValidationErrors = make([]*validation.ValidationError, 0, len(vErrs))
		for _, validationErr := range vErrs {
			cast.ValidationErrors = append(cast.ValidationErrors, castValidationError(validationErr))
		}
	}
	return cast
}

// castValidationError casts a `ValidationError` from the validator package
// into the `ValidationError` type expected in the API return type.
func castValidationError(vErr validator.ValidationError) *validation.ValidationError {
	cast := &validation.ValidationError{
		Field:       vErr.Field(),
		Type:        vErr.Type(),
		Context:     vErr.Context().String(),
		Description: vErr.Description(),
		Value:       vErr.Value(),
		Details:     vErr.Details(),
	}
	return cast
}
