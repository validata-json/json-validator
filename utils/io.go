package utils

import (
	"io/ioutil"
	"net/http"
	"os"
	"path"

	"github.com/pkg/errors"
)

// fetchContent checks if the uri points towards a local or remote ressource
// and tries to get its content.
func FetchContent(uri string) (string, error) {
	var content string
	var err error
	if isValidFile(uri) {
		content, err = FetchLocalFile(uri)
	} else {
		content, err = FetchRemoteFile(uri)
	}
	return content, err
}

func FetchRemoteFile(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", errors.Wrapf(err, "An error occurred while fetching remote file %s", path.Base(url))
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.Wrapf(err, "An error occurred while reading remote file %s", path.Base(url))
	}
	return string(content), nil
}

func FetchLocalFile(path string) (string, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return "", errors.Wrapf(err, "An error occurred while reading local file %s", path)
	}

	return string(content), nil
}

func isValidFile(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}
