package validationapi

import (
	"context"
	validation "json-validator/gen/validation"
	"json-validator/utils"
	"json-validator/validator"
	"log"
)

// validation service example implementation.
// The example methods log the requests and return zero values.
type validationsrvc struct {
	logger *log.Logger
}

// NewValidation returns the validation service implementation.
func NewValidation(logger *log.Logger) validation.Service {
	return &validationsrvc{logger}
}

func (s *validationsrvc) ValidateFile(ctx context.Context, p *validation.ValidateFilePayload) (res *validation.ValidationResult, err error) {
	data := p.Data
	schema, err := utils.FetchRemoteFile(p.Schema)
	if err != nil {
		return nil, err
	}
	report, err := validator.Validate(schema, data)
	includeRawErrors := p.Raw
	return utils.CastValidationErrors(report, includeRawErrors), err
}

// ValidateURL implements validate-url.
func (s *validationsrvc) ValidateURL(ctx context.Context, p *validation.ValidateURLPayload) (res *validation.ValidationResult, err error) {
	schema, err := utils.FetchRemoteFile(p.Schema)
	if err != nil {
		return nil, err
	}
	data, err := utils.FetchRemoteFile(p.Data)
	if err != nil {
		return nil, err
	}

	errors, err := validator.Validate(schema, data)
	includeRawErrors := p.Raw
	return utils.CastValidationErrors(errors, includeRawErrors), err
}
