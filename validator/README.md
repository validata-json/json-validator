# Module de validation de données JSON

Ce module implémente la partie métier de la validation des données JSON, 
c'est-à-dire la traduction des erreurs et la création d'un rapport adressé à 
des humains.

La validation s'appuie sur 
[gojsonschema](https://github.com/xeipuuv/gojsonschema).

Un example de rapport de validation complet peut être trouvé dans le fichier 
[./validator_test.go](./validator_test.go).

Le fichier d'exemples (testé) 
[./error_formatting_test.go](./error_formatting_test.go) est généré selon les 
données présentes dans 
[./data/examples/generated/](./data/examples/generated).
Le processus de développement utilisé pour l'amélioration des messages 
d'erreur est le suivant :
- Vérifier que les exemples du fichier ./error_formatting_test.go fonctionnent 
  avec `go test ./...`.
- Rajouter des fichiers d'exemple dans le répertoire 
  "./data/examples/generated/" (un fichier "*_data.json" et un fichier 
  "*_schema.json".
- Générer le nouveau fichier test avec `go generate ./...`.
- Modifier au besoin le texte de rapport qui a été généré pour le nouvel 
  exemple (vérifier que les tests ne passent plus).
- Modifier le code jusqu'à ce que le test du nouvel exemple passe (sans 
  regénérer).
