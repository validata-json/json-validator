package validator

import (
	"encoding/json"
	"fmt"
	"sort"
)

// Run go generate to generate testable examples of validation error reports,
// with schema and data files in "validator/data/examples/generated" folder,
// and output in the file "error_formatting_test.go".

//go:generate go run example_generator/example_generator.go --dir ./data/examples/generated --out error_formatting_test.go --template ./error_formatting_test.template

func validationErrorString(vErr ValidationError) string {
	return fmt.Sprintf(`Erreur survenue au niveau de l'attribut "%s"
Position                : %s
Description de l'erreur : %s
Valeur de l'objet       : %s
Code erreur             : %s`,
		vErr.Field(),
		vErr.ContextString(),
		vErr.Description(),
		toTruncatedJSON(vErr.Value()),
		vErr.Type())
}

func validationReportString(report ValidationReport) string {
	str := ""
	counter := 0
	reportSlice := report.toSlice()
	sort.Sort(reportSlice)
	for _, entry := range reportSlice {
		str += fmt.Sprintf("%s\n", entry.sampleError.String())
		if entry.occurrence > 1 {
			str += fmt.Sprintf(`Erreur similaire sur cet attribut constatée à %d autre(s) position(s)
`, entry.occurrence-1)
			if entry.occurrence <= MAX_REPORTED_CONTEXTS {
				for _, context := range entry.firstContexts[1:] {
					str += fmt.Sprintln("	" + context)
				}
			}
		}

		if counter < len(report)-1 {
			str += "\n"
		}
		counter++
	}
	return str
}

func toTruncatedJSON(obj interface{}) string {
	jsonBytes, err := json.Marshal(obj)
	if err != nil {
		return fmt.Sprintf("%+v", obj)
	}
	jsonString := string(jsonBytes)
	if len(jsonString) > MAX_VALUE_STRING_LENGTH {
		jsonString = jsonString[:MAX_VALUE_STRING_LENGTH] + TRUNCATION_STRING
	}
	return jsonString
}
