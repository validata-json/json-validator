package main

import (
	"flag"
	"io"
	"io/ioutil"
	"json-validator/validator"
	"log"
	"os"
	"path"
	"strings"
	"text/template"

	"github.com/iancoleman/strcase"
)

type ExampleData struct {
	Dir      string
	Examples []Example
}

type Example struct {
	MethodName string
	Schema     string
	Data       string
	Output     string
}

func main() {
	dirF := flag.String("dir", path.Join("..", "..", "data", "examples"), "Directory of example data.")
	templateF := flag.String("template", path.Join("..", "error_formatting_test.template"), "Template file")
	outputFileF := flag.String("out", "out.go", "Ouput file path")
	flag.Parse()

	dir := *dirF
	templatePath := *templateF
	outputFile := *outputFileF

	gd := newExampleData(dir)

	f, err := os.Create(outputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	fillTemplate(f, templatePath, gd)
}

// newExampleData reads example data in `dir` and stores it. Schema files are
// expected to end with "_schema.json" and data files with "_data.json". The
// rest of the name should be identical for the rest of the name for the
// schema and data to be paired together.
func newExampleData(dir string) ExampleData {
	files, err := os.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	examplesMap := make(map[string]Example)
	for _, file := range files {
		fileName := file.Name()
		exampleName := strings.TrimSuffix(fileName, "_schema.json")
		exampleName = strings.TrimSuffix(exampleName, "_data.json")
		if _, ok := examplesMap[exampleName]; !ok {
			schemaPath := path.Join(dir, exampleName+"_schema.json")
			dataPath := path.Join(dir, exampleName+"_data.json")

			schema, err := ioutil.ReadFile(schemaPath)
			if err != nil {
				log.Fatal(err)
			}
			data, err := ioutil.ReadFile(dataPath)
			if err != nil {
				log.Fatal(err)
			}
			validationErrors, err := validator.Validate(string(schema), string(data))
			output := validationErrors.GenerateReport()
			examplesMap[exampleName] = Example{
				MethodName: strcase.ToCamel(exampleName),
				Schema:     string(schema),
				Data:       string(data),
				Output:     output,
			}
		}
	}
	examples := make([]Example, 0, len(examplesMap))
	for _, ex := range examplesMap {
		examples = append(examples, ex)
	}
	return ExampleData{Dir: dir, Examples: examples}
}

func fillTemplate(w io.Writer, templatePath string, data ExampleData) {
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		log.Fatal(err)
	}
	t.Execute(w, data)
}
