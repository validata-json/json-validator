package validator

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"
)

const (
	NO_FILTER int = iota
	FILTER_VALID
	FILTER_INVALID
)

type TestSuite struct {
	Description string      `json:"description"`
	Schema      interface{} `json:"schema"`
	Tests       []TestCase  `json:"tests"`
}

type TestCase struct {
	Description string      `json:"description"`
	Data        interface{} `json:"data"`
	DataIsValid bool        `json:"valid"`
}

func readAndUnmarshalTestSuite(t *testing.T, path string) []TestSuite {
	t.Helper()
	jsonFile, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	var testData []TestSuite
	defer jsonFile.Close()
	jsonBytes, _ := io.ReadAll(jsonFile)
	json.Unmarshal(jsonBytes, &testData)
	return testData
}

type ValidationTest func(t *testing.T, description string, report ValidationErrors, err error)

// runForEachTestCase runs a test on each Test Case of a test suite, after possibly filtering
func runForEachTestCase(t *testing.T, testFunction ValidationTest, testSuite TestSuite, validityFilter int) {
	t.Helper()
	schema, _ := json.Marshal(testSuite.Schema)
	for _, test := range testSuite.Tests {
		if shouldRunTest(test.DataIsValid, validityFilter) {
			data, _ := json.Marshal(test.Data)
			report, err := Validate(string(schema), string(data))
			testFunction(t, test.Description, report, err)
		}
	}
}

// runForEachTestSuite reads all files in the "dirPath" directory into
// `TestSuite`s, and runs a ValidationTest on each TestCase of every
// TestSuite.
func runForEachTestSuite(t *testing.T, testFunction ValidationTest, dirPath string, validityFilter int) {
	t.Helper()
	testFiles, err := ioutil.ReadDir(dirPath)
	if err != nil {
		log.Fatal(err)
	}
	for _, testFile := range testFiles {
		if !testFile.IsDir() {
			path := filepath.Join(dirPath, testFile.Name())
			t.Log("Running Tests at: " + path)
			for _, testSuite := range readAndUnmarshalTestSuite(t, path) {
				t.Log("TestSuite: " + testSuite.Description)
				runForEachTestCase(t, testFunction, testSuite, validityFilter)
			}
		}
	}
}

func shouldRunTest(dataIsValid bool, validityFilter int) bool {
	if validityFilter == NO_FILTER {
		return true
	}
	if dataIsValid && validityFilter == FILTER_VALID {
		return true
	}
	if !dataIsValid && validityFilter == FILTER_INVALID {
		return true
	}
	return false
}
