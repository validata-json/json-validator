package validator

var type_translations = map[string]string{
	"array":   `tableau`,
	"boolean": `booléen`,
	"integer": `nombre entier`,
	"number":  `nombre décimal`,
	"null":    `type "null"`,
	"object":  `objet`,
	"string":  `chaîne de caractères`,
}

// translate offers type translations for template parameters
func translate(s string) string {
	if t, ok := type_translations[s]; ok {
		return t
	}
	return s
}

type FrenchLocale struct{}

// False returns a format-string for "false" schema validation errors
func (l *FrenchLocale) False() string {
	return "False always fails validation"
}

// Required returns a format-string for "required" schema validation errors
func (l FrenchLocale) Required() string {
	return `La propriété "{{.property}}" est manquante alors qu'elle est obligatoire`
}

// InvalidType returns a format-string for "invalid type" schema validation errors
func (l *FrenchLocale) InvalidType() string {
	return `Type invalide. Un(e) {{translate .given}} a été trouvé(e), alors qu'un(e) {{translate .expected}} est attendu(e).`
}

// NumberAnyOf returns a format-string for "anyOf" schema validation errors
func (l *FrenchLocale) NumberAnyOf() string {
	return `Au moins l'un des schémas doit être validé (mot-clé : anyOf)`
}

// NumberOneOf returns a format-string for "oneOf" schema validation errors
func (l *FrenchLocale) NumberOneOf() string {
	return `Un des schémas, et seulement un, doit être validé (mot-clé : oneOf)`
}

// NumberAllOf returns a format-string for "allOf" schema validation errors
func (l *FrenchLocale) NumberAllOf() string {
	return `Tous les schémas doivent être validés (mot-clé : allOf)`
}

// NumberNot returns a format-string to format a NumberNotError
func (l *FrenchLocale) NumberNot() string {
	return `Le schéma ne doit pas être validé (mot-clé : not)`
}

// MissingDependency returns a format-string for "missing dependency" schema validation errors
func (l *FrenchLocale) MissingDependency() string {
	return `Dépendance à {{.dependency}}`
}

// Internal returns a format-string for internal errors
func (l *FrenchLocale) Internal() string {
	return `Erreur interne {{.error}}`
}

// Const returns a format-string to format a ConstError
func (l *FrenchLocale) Const() string {
	return `{{.field}} ne correspond pas à : {{.allowed}}`
}

// Enum returns a format-string to format an EnumError
func (l *FrenchLocale) Enum() string {
	return `{{.field}} doit prendre l'une des valeurs suivantes : {{.allowed}}`
}

// ArrayNoAdditionalItems returns a format-string to format an ArrayNoAdditionalItemsError
func (l *FrenchLocale) ArrayNoAdditionalItems() string {
	return `Le tableau contient des éléments supplémentaires à ceux autorisés par le schéma.`
}

// ArrayNotEnoughItems returns a format-string to format an error for arrays having not enough items to match positional list of schema
func (l *FrenchLocale) ArrayNotEnoughItems() string {
	return `Pas assez d'éléments dans le tableau pour correspondre à la liste positionnelle du schéma`
}

// ArrayMinItems returns a format-string to format an ArrayMinItemsError
func (l *FrenchLocale) ArrayMinItems() string {
	return `Le tableau doit avoir au moins {{.min}} éléments`
}

// ArrayMaxItems returns a format-string to format an ArrayMaxItemsError
func (l *FrenchLocale) ArrayMaxItems() string {
	return `Le tableau doit avoir au plus {{.max}} éléments`
}

// Unique returns a format-string  to format an ItemsMustBeUniqueError
func (l *FrenchLocale) Unique() string {
	return `{{.type}} items[{{.i}},{{.j}}] doit être unique`
}

// ArrayContains returns a format-string to format an ArrayContainsError
func (l *FrenchLocale) ArrayContains() string {
	return `Au moins l'un des éléments doit correspondre`
}

// ArrayMinProperties returns a format-string to format an ArrayMinPropertiesError
func (l *FrenchLocale) ArrayMinProperties() string {
	return `Au moins {{.min}} attributs requis`
}

// ArrayMaxProperties returns a format-string to format an ArrayMaxPropertiesError
func (l *FrenchLocale) ArrayMaxProperties() string {
	return `Au plus {{.max}} attributs autorisés`
}

// AdditionalPropertyNotAllowed returns a format-string to format an AdditionalPropertyNotAllowedError
func (l *FrenchLocale) AdditionalPropertyNotAllowed() string {
	return `L'objet possède un attribut, "{{.property}}", qui n'est pas autorisé par le schéma.`
}

// InvalidPropertyPattern returns a format-string to format an InvalidPropertyPatternError
func (l *FrenchLocale) InvalidPropertyPattern() string {
	return `L'attribut "{{.property}}" ne respecte pas le motif {{.pattern}}`
}

// InvalidPropertyName returns a format-string to format an InvalidPropertyNameError
func (l *FrenchLocale) InvalidPropertyName() string {
	return `Le nom de l'attribut "{{.property}}" ne correspond pas`
}

// StringGTE returns a format-string to format an StringLengthGTEError
func (l *FrenchLocale) StringGTE() string {
	return `La chaîne de caractères doit être au moins de longueur {{.min}}`
}

// StringLTE returns a format-string to format an StringLengthLTEError
func (l *FrenchLocale) StringLTE() string {
	return `La chaîne de caractères doit être au plus de longueur {{.max}}`
}

// DoesNotMatchPattern returns a format-string to format an DoesNotMatchPatternError
func (l *FrenchLocale) DoesNotMatchPattern() string {
	return `Non-respect du motif : '{{.pattern}}'`
}

// DoesNotMatchFormat returns a format-string to format an DoesNotMatchFormatError
func (l *FrenchLocale) DoesNotMatchFormat() string {
	return `Non-respect du format : '{{.format}}'`
}

// MultipleOf returns a format-string to format an MultipleOfError
func (l *FrenchLocale) MultipleOf() string {
	return `Multiple de {{.multiple}} requis`
}

// NumberGTE returns the format string to format a NumberGTEError
func (l *FrenchLocale) NumberGTE() string {
	return `Nombre supérieur ou égal à {{.min}} requis`
}

// NumberGT returns the format string to format a NumberGTError
func (l *FrenchLocale) NumberGT() string {
	return `Nombre strictement supérieur à {{.min}} requis`
}

// NumberLTE returns the format string to format a NumberLTEError
func (l *FrenchLocale) NumberLTE() string {
	return `Nombre inférieur ou égal à {{.max}} requis`
}

// NumberLT returns the format string to format a NumberLTError
func (l *FrenchLocale) NumberLT() string {
	return `Nombre strictement inférieur à {{.max}} requis`
}

// Schema validators

// RegexPattern returns a format-string to format a regex-pattern error
func (l *FrenchLocale) RegexPattern() string {
	return `Motif de l'expression régulière (regex) invalide : '{{.pattern}}'`
}

// GreaterThanZero returns a format-string to format an error where a number must be greater than zero
func (l *FrenchLocale) GreaterThanZero() string {
	return `{{.number}} doit être strictement positif`
}

// MustBeOfA returns a format-string to format an error where a value is of the wrong type
func (l *FrenchLocale) MustBeOfA() string {
	return `{{.x}} doit être un {{.y}}`
}

// MustBeOfAn returns a format-string to format an error where a value is of the wrong type
func (l *FrenchLocale) MustBeOfAn() string {
	return `{{.x}} doit être un {{.y}}`
}

// CannotBeUsedWithout returns a format-string to format a "cannot be used without" error
func (l *FrenchLocale) CannotBeUsedWithout() string {
	return `{{.x}} ne peut pas être utilisé sans {{.y}}`
}

// CannotBeGT returns a format-string to format an error where a value are greater than allowed
func (l *FrenchLocale) CannotBeGT() string {
	return `{{.x}} ne peut pas être supérieur à {{.y}}`
}

// MustBeOfType returns a format-string to format an error where a value does not match the required type
func (l *FrenchLocale) MustBeOfType() string {
	return `{{.key}} doit être du type {{.type}}`
}

// MustBeValidRegex returns a format-string to format an error where a regex is invalid
func (l *FrenchLocale) MustBeValidRegex() string {
	return `{{.key}} doit être une expression régulière valide`
}

// MustBeValidFormat returns a format-string to format an error where a value does not match the expected format
func (l *FrenchLocale) MustBeValidFormat() string {
	return `{{.key}} doit être un format valide {{.given}}`
}

// MustBeGTEZero returns a format-string to format an error where a value must be greater or equal than 0
func (l *FrenchLocale) MustBeGTEZero() string {
	return `{{.key}} doit être positif ou nul`
}

// KeyCannotBeGreaterThan returns a format-string to format an error where a value is greater than the maximum  allowed
func (l *FrenchLocale) KeyCannotBeGreaterThan() string {
	return `{{.key}} ne peut pas être supérieur à {{.y}}`
}

// KeyItemsMustBeOfType returns a format-string to format an error where a key is of the wrong type
func (l *FrenchLocale) KeyItemsMustBeOfType() string {
	return `Les éléments de {{.key}} doivent être de type {{.type}}`
}

// KeyItemsMustBeUnique returns a format-string to format an error where keys are not unique
func (l *FrenchLocale) KeyItemsMustBeUnique() string {
	return `{{.key}} items must be unique`
}

// ReferenceMustBeCanonical returns a format-string to format a "reference must be canonical" error
func (l *FrenchLocale) ReferenceMustBeCanonical() string {
	return `La réference {{.reference}} doit être canonique`
}

// NotAValidType returns a format-string to format an invalid type error
func (l *FrenchLocale) NotAValidType() string {
	return `Le type primitif est invalide -- Obtenu : {{.given}} Valeurs valides attendues :{{.expected}}`
}

// Duplicated returns a format-string to format an error where types are duplicated
func (l *FrenchLocale) Duplicated() string {
	return `{{.type}} est dupliqué`
}

// HttpBadStatus returns a format-string for errors when loading a schema using HTTP
func (l *FrenchLocale) HttpBadStatus() string {
	return `La lecture du schéma via HTTP a échoué, le statut de la réponse est {{.status}}`
}

// ErrorFormat returns a format string for errors
// Replacement options: field, description, context, value
func (l *FrenchLocale) ErrorFormat() string {
	return `{{.field}}: {{.description}}`
}

// ParseError returns a format-string for JSON parsing errors
func (l *FrenchLocale) ParseError() string {
	return `Attendu: {{.expected}}, obtenu: JSON invalide`
}

// ConditionThen returns a format-string for ConditionThenError errors
// If/Else
func (l *FrenchLocale) ConditionThen() string {
	return `Doit valider le schéma du mot-clé "then" comme la condition du mot-clé "if" est validée`
}

// ConditionElse returns a format-string for ConditionElseError errors
func (l *FrenchLocale) ConditionElse() string {
	return `Doit valider le schéma du mot-clé "else" comme la condition du mot-clé "if" n'est pas validée `
}
