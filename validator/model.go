package validator

import (
	"regexp"
	"strings"

	"github.com/xeipuuv/gojsonschema"
)

// constants
const (
	// STRING_NUMBER = "nombre décimal"
	// STRING_ARRAY_OF_STRINGS           = "tableau de chaînes de caractères"
	// STRING_ARRAY_OF_SCHEMAS           = "tableau de schémas"
	// STRING_SCHEMA                     = "schéma valide"
	// STRING_SCHEMA_OR_ARRAY_OF_STRINGS = "schéma ou tableau de chaînes de caractères"
	// STRING_PROPERTIES                 = "attributs"
	// STRING_DEPENDENCY                 = "dépendance"
	// STRING_PROPERTY                   = "attribut"
	// STRING_UNDEFINED                  = "non défini"
	STRING_CONTEXT_ROOT         = "(racine)"
	STRING_ROOT_SCHEMA_PROPERTY = "(attribut sans nom)"
	TRUNCATION_STRING           = "...OBJET TRONQUÉ"
	MAX_VALUE_STRING_LENGTH     = 43
	MAX_REPORTED_CONTEXTS       = 5
)

// ValidationError is a type that represents a validation error. It embeds
// gojsonschema.ResultErrorFields.
type ValidationError struct {
	gojsonschema.ResultError
}

func (vErr ValidationError) String() string {
	return validationErrorString(vErr)
}

func (vErr ValidationError) ContextString() string {
	var context string
	if vErr.Context() == nil {
		context = ""
	} else {
		context = vErr.Context().String()
	}
	return formatFrContext(context)
}

func (vErr ValidationError) Field() string {
	field := vErr.ResultError.Field()
	return formatFrField(field)
}

func formatFrContext(context string) string {
	context = strings.ReplaceAll(context, gojsonschema.STRING_CONTEXT_ROOT, STRING_CONTEXT_ROOT)
	return context
}

func formatFrField(field string) string {
	field = strings.ReplaceAll(field, gojsonschema.STRING_ROOT_SCHEMA_PROPERTY, STRING_ROOT_SCHEMA_PROPERTY)
	field_segments := strings.Split(field, ".")
	field = field_segments[len(field_segments)-1]
	return field
}

type ValidationErrors []ValidationError

func (vErrs ValidationErrors) HasAnyError() bool {
	return len(vErrs) >= 1
}

type ValidationReportEntry struct {
	occurrence    int
	sampleError   ValidationError
	firstContexts []string
}

type TypeAndContext struct {
	// errorType is the validation error type
	errorType string
	// sameArrayContext is the context of a property where array indexes are
	// replaced with `*`. As a result, same properties for different array
	// elements will have equal "sameArrayContext".
	sameArrayContext string
}

type ValidationReport map[TypeAndContext]*ValidationReportEntry

// GenerateReport creates a human readale report, which tries to gather
// similar errors together in a single paragraph.
func (vErrs ValidationErrors) GenerateReport() string {
	if len(vErrs) == 0 {
		return "Les données sont valides par rapport au schéma JSON"
	} else if len(vErrs) == 1 {
		return vErrs[0].String()
	}

	report := make(ValidationReport)
	for _, vErr := range vErrs {
		key := TypeAndContext{vErr.Type(), stripToSameArrayContext(vErr.ContextString())}
		reportEntry, noEntryYet := report[key]
		if !noEntryYet {
			report[key] = newValidationReportEntry(vErr)
		} else {
			reportEntry.Add(vErr.ContextString())
		}
	}
	return report.String()
}

func stripToSameArrayContext(context string) string {
	arraysInContext := regexp.MustCompile(`\.[0-9]+\.`)
	return arraysInContext.ReplaceAllString(context, ".*.")
}

func newValidationReportEntry(validationError ValidationError) *ValidationReportEntry {
	return &ValidationReportEntry{1, validationError, []string{validationError.ContextString()}}
}

func (e *ValidationReportEntry) Add(context string) {
	if e.occurrence <= MAX_REPORTED_CONTEXTS {
		e.firstContexts = append(e.firstContexts, context)
	}
	e.occurrence = e.occurrence + 1
}

// ValidationReport implements the Stringer interface
func (rep ValidationReport) String() string {
	return validationReportString(rep)
}

// validationReportSlice is an auxiliary type that allows the ValidationReport
// to be ordered.
type validationReportSlice []*ValidationReportEntry

func (rep ValidationReport) toSlice() validationReportSlice {
	slice := make([]*ValidationReportEntry, 0, len(rep))
	for _, v := range rep {
		slice = append(slice, v)
	}
	return slice
}

// ValidationReportSlice implements the sort.Interface interface
// Sorting a validation Report will sort by lexical order of contexts of the
// first occurrence of a specific validationReportEntry.
func (report validationReportSlice) Len() int { return len(report) }
func (report validationReportSlice) Less(i, j int) bool {
	if len(report[i].firstContexts) == 0 || len(report[j].firstContexts) == 0 {
		// should never occur, as ValidationReportEntry are initialized with
		// non-empty `firstContexts` attributes.
		panic("An invalid ValidationReportEntry has been encountered (empty `firstContexts` field)")
	}
	return report[i].firstContexts[0] < report[j].firstContexts[0]
}
func (report validationReportSlice) Swap(i, j int) { report[i], report[j] = report[j], report[i] }
