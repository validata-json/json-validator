package validator

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xeipuuv/gojsonschema"
)

func makeValidationError(Type, Context string) ValidationError {
	resultError := gojsonschema.ResultErrorFields{}
	resultError.SetType(Type)
	resultError.SetContext(gojsonschema.NewJsonContext(Context, nil))
	return ValidationError{&resultError}

}
func TestValidationError(t *testing.T) {
	vErr := makeValidationError("", "")
	assert.IsType(t, vErr.String(), "")
}

func TestGenerateReport(t *testing.T) {
	t.Run("No errors returns explicit message", func(t *testing.T) {
		noError := ValidationErrors{}
		msg := noError.GenerateReport()
		assert.Equal(t, "Les données sont valides par rapport au schéma JSON", msg)
	})

	t.Run("A unique error is returned as is", func(t *testing.T) {
		oneError := ValidationErrors{
			makeValidationError("invalid_type", "(racine).features.0.properties.nom_loc"),
		}
		msg := oneError.GenerateReport()
		assert.Equal(t, oneError[0].String(), msg)
	})
	t.Run("Two errors of the same type, where position in array is the only difference, are summarized", func(t *testing.T) {
		errors := ValidationErrors{
			makeValidationError("invalid_type", "(racine).features.1.properties.nom_loc"),
			makeValidationError("invalid_type", "(racine).features.1.properties.nom_loc"),
		}
		msg := errors.GenerateReport()
		assert.Equal(t, fmt.Sprintf(
			`%s
Erreur similaire sur cet attribut constatée à 1 autre(s) position(s)
	(racine).features.1.properties.nom_loc
`,
			errors[0]),
			msg)
	})
}
