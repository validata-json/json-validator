package validator

import (
	"github.com/xeipuuv/gojsonschema"
)

// Validate takes a json schema and json data as strings, and returns the
// validation results
func Validate(schemaStr, dataStr string) (ValidationErrors, error) {

	// Error formatting
	gojsonschema.Locale = &FrenchLocale{}
	gojsonschema.ErrorTemplateFuncs = map[string]interface{}{
		"translate": translate,
	}

	// Prepare schema and data
	schemaLoader := gojsonschema.NewStringLoader(schemaStr)
	schema, err := gojsonschema.NewSchema(schemaLoader)
	if err != nil {
		return nil, err
	}

	dataLoader := gojsonschema.NewStringLoader(dataStr)

	// Validation and report
	res, err := schema.Validate(dataLoader)
	if err != nil {
		return nil, err
	}

	resultErrors := res.Errors()
	validationErrors := make(ValidationErrors, len(resultErrors))
	for index, resultError := range resultErrors {
		validationErrors[index] = ValidationError{ResultError: resultError}
	}
	return validationErrors, nil
}
