package validator

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var schemaAmenagementsCyclables = readJSONFile(filepath.Join("data", "examples", "schema-amenagements-cyclable.json"))
var validDataAmenagementsCyclables = readJSONFile(filepath.Join("data", "examples", "france-20220301-valid-subset.geojson"))
var failingDataAmenagementsCyclables = readJSONFile(filepath.Join("data", "examples", "france-20220301-failing-subset.geojson"))

var TEST_DATA_DIR = filepath.Join("data", "tests")

func readJSONFile(path string) string {
	slurp, err := os.ReadFile(path)
	if err != nil {
		panic(errors.New("Unexpected error while reading test file"))
	}
	return string(slurp)
}

// TestUnprocessableJsonSchema tests if a test with an unprocessable schema
// returns an error
func TestUnprocessableJsonSchema(t *testing.T) {
	wrongSpec := strings.Replace(schemaAmenagementsCyclables, `"type": "number"`, `"type": 0`, 1)
	_, err := Validate(wrongSpec, validDataAmenagementsCyclables)
	if err == nil {
		t.FailNow()
	}
}

// TestValidFiles tests if a valid file is detected as such on a json schema
// test suite
func TestValidFiles(t *testing.T) {
	t.Run("Test validator on generic test suite", func(t *testing.T) {
		runForEachTestSuite(t, dataIsValid, TEST_DATA_DIR, FILTER_VALID)
	})
	t.Run("Test validator on business data", func(t *testing.T) {
		report, err := Validate(schemaAmenagementsCyclables, validDataAmenagementsCyclables)
		dataIsValid(t, "", report, err)
	})
}

// TestInvalidFile tests if an invalid file is detected as such and returns
// the appropriate errors in the validation report
func TestInvalidFile(t *testing.T) {
	t.Run("Test validator on generic test suite", func(t *testing.T) {
		runForEachTestSuite(t, dataIsInvalid, TEST_DATA_DIR, FILTER_INVALID)
	})
}

func dataIsValid(t *testing.T, description string, validationErrors ValidationErrors, err error) {
	t.Helper()
	assert.Equal(t, nil, err, "The test \""+description+"\" should run without error")
	assert.Equal(t, false, validationErrors.HasAnyError(),
		fmt.Sprintf("The data of test \"%s\" should be validated.", description))
	assert.Equal(t, 0, len(validationErrors),
		fmt.Sprintf("No validation error is expected for test \"%s\"", description))
}

func dataIsInvalid(t *testing.T, description string, validationErrors ValidationErrors, err error) {
	t.Helper()
	assert.Equal(t, nil, err, "The test \""+description+"\" should run without error")
	assert.Equal(t, true, validationErrors.HasAnyError(),
		fmt.Sprintf("The data of test \"%s\" should NOT be validated.", description))
	assert.Greater(t, len(validationErrors), 0,
		fmt.Sprintf("At least one validation error is expected for test \"%s\"", description))
}

func Example() {
	schema := schemaAmenagementsCyclables
	data := failingDataAmenagementsCyclables
	report, _ := Validate(schema, data)
	fmt.Println(report.GenerateReport())
	// Output: Erreur survenue au niveau de l'attribut "access_ame"
	// Position                : (racine).features.0.properties.access_ame
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 60 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "ame_d"
	// Position                : (racine).features.0.properties.ame_d
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 23 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "ame_g"
	// Position                : (racine).features.0.properties.ame_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 23 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "comm"
	// Position                : (racine).features.0.properties.comm
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 62 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "d_service"
	// Position                : (racine).features.0.properties.d_service
	// Description de l'erreur : Type invalide. Un(e) chaîne de caractères a été trouvé(e), alors qu'un(e) nombre décimal est attendu(e).
	// Valeur de l'objet       : "inconnue"
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 62 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "id_osm"
	// Position                : (racine).features.0.properties.id_osm
	// Description de l'erreur : Type invalide. Un(e) nombre entier a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : 644910645
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 62 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "largeur_d"
	// Position                : (racine).features.0.properties.largeur_d
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) nombre décimal est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 59 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "largeur_g"
	// Position                : (racine).features.0.properties.largeur_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) nombre décimal est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 59 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "local_d"
	// Position                : (racine).features.0.properties.local_d
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 54 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "local_g"
	// Position                : (racine).features.0.properties.local_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 55 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "lumiere"
	// Position                : (racine).features.0.properties.lumiere
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) booléen est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 49 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "nom_loc"
	// Position                : (racine).features.0.properties.nom_loc
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) tableau est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 62 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "num_iti"
	// Position                : (racine).features.0.properties.num_iti
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 61 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "reseau_loc"
	// Position                : (racine).features.0.properties.reseau_loc
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 62 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "revet_d"
	// Position                : (racine).features.0.properties.revet_d
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 36 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "revet_g"
	// Position                : (racine).features.0.properties.revet_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 36 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "regime_d"
	// Position                : (racine).features.1.properties.regime_d
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 2 autre(s) position(s)
	// 	(racine).features.13.properties.regime_d
	// 	(racine).features.20.properties.regime_d
	//
	// Erreur survenue au niveau de l'attribut "regime_g"
	// Position                : (racine).features.1.properties.regime_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 7 autre(s) position(s)
	//
	// Erreur survenue au niveau de l'attribut "trafic_vit"
	// Position                : (racine).features.1.properties.trafic_vit
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) nombre entier est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 3 autre(s) position(s)
	// 	(racine).features.4.properties.trafic_vit
	// 	(racine).features.13.properties.trafic_vit
	// 	(racine).features.20.properties.trafic_vit
	//
	// Erreur survenue au niveau de l'attribut "sens_g"
	// Position                : (racine).features.9.properties.sens_g
	// Description de l'erreur : Type invalide. Un(e) type "null" a été trouvé(e), alors qu'un(e) chaîne de caractères est attendu(e).
	// Valeur de l'objet       : null
	// Code erreur             : invalid_type
	// Erreur similaire sur cet attribut constatée à 4 autre(s) position(s)
	// 	(racine).features.13.properties.sens_g
	// 	(racine).features.32.properties.sens_g
	// 	(racine).features.39.properties.sens_g
	// 	(racine).features.52.properties.sens_g

}
